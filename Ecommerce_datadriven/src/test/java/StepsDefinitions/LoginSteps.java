package StepsDefinitions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.Keys;
import io.cucumber.java.en.*;
import io.github.bonigarcia.wdm.WebDriverManager;

public class LoginSteps {

	WebDriver driver;

	@Given("utilisateur est sur la page login")
	public void utilisateur_est_sur_la_page_login() {

		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://tutorialsninja.com/demo/index.php?route=account/login");
		System.out.println("l'utilisateur est connecté a la page login");
	}

	@When("^utilisateur saisie (.*) et (.*)$")
	public void utilisateur_saisie_email_et_password(String email, String password) {
		System.out.println("l'utilisateur saisie ces donneés");
		driver.findElement(By.id("input-email")).sendKeys(email);
		driver.findElement(By.id("input-password")).sendKeys(password);
	}

	@And("click sur le bouton login")
	public void click_sur_le_bouton_login() {
		System.out.println("l'utilisateur click sur le boutton login");
		driver.findElement(By.id("input-password")).sendKeys(Keys.ENTER);
	}

	@Then("il se connecte")
	public void il_se_connecte() {
		if(driver.getPageSource().contains("Warning: No match for E-Mail Address and/or Password.")){
			System.out.println("donnees invalid!");
			}else{
			System.out.println("connection avec succes!");
			}
		
	}
}
