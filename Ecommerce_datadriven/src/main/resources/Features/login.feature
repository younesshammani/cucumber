#rediger par youness hammani
Feature: feature pour login test

  @test_login
  Scenario Outline: verifier le login avec des donnees valides/invalides
    Given utilisateur est sur la page login
    When utilisateur saisie <email> et <password>
    And click sur le bouton login
    Then il se connecte

    Examples: 
      | email              | password |
      | 2123185@bdeb.qc.ca | 12345678 |
      | 2123185@bdeb.qc.ca | 87654321 |
