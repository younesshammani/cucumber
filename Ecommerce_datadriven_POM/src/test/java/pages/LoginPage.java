package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage {
	WebDriver driver;

	By txt_email = By.xpath("//input[@id='input-email']");

	By txt_password = By.xpath("//input[@id='input-password']");

	By btn_login = By.xpath("//input[@value='Login']");

	public LoginPage(WebDriver driver) {
		this.driver = driver;

	}

	public void enterEmail(String email) {
		driver.findElement(txt_email).sendKeys(email);
	}

	public void enterPassword(String password) {
		driver.findElement(txt_password).sendKeys(password);
	}

	public void clickLogin() {
		driver.findElement(btn_login).click();
	}

	public void loginDatavalid(String email, String password) {
		driver.findElement(txt_email).sendKeys(email);
		driver.findElement(txt_password).sendKeys(password);
		driver.findElement(btn_login).click();
	}

}
