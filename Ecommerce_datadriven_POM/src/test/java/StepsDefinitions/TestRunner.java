package StepsDefinitions;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features ="src\\main\\resources\\Features",
glue = {"StepsDefinitions"},
monochrome = true
//tags="@test_login"
		)
public class TestRunner {
}
